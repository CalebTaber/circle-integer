# Circle-Integer
An integer data structure that has a rollover function outside of defined bounds

Example:
A CircleInt is defined with initial value of 3, negative bound of -7, and positive bound of 11.
Adding 10 to the CircleInt would result in a value of -4 because 13 is larger than the positive bound, so it rolls around to the negative bound.