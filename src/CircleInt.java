package main;

public class CircleInt {

    private int value, negBound, posBound, range;

    public CircleInt(int init, int negBound, int posBound) {
        value = init;
        this.negBound = negBound;
        this.posBound = posBound;
        range = posBound - negBound;
        rollover();
    }

    private void rollover() {
        if (value > posBound) {
            value -= posBound; // Subtract the posBound to start the value at the negBound
            value %= range; // Basically subtracts range until the value is less than the range
            value += negBound - 1; // Add the result to the negBound to get the final value
        } else if (value < negBound) {
            value -= negBound; // Subtract negBound to start the value at posBound
            value %= range; // Basically subtracts range until the value is less than the range
            value += posBound + 1; // Add the result to posBound to get the final value
        }
    }

    public void add(int i) {
        value += i;
        rollover();
        System.out.println(toString());
    }

    public void subtract(int i) {
        value -= i;
        rollover();
    }

    public void multiply(int i) {
        value *= i;
        rollover();
    }

    public void divide(int i) {
        try { // Try-catch for DivideByZero errors
            value /= i;
        } catch(Exception e) {
            e.printStackTrace();
        }
        rollover();
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return "value: " + value + "\tNegBound: " + negBound + "\tPosBound: " + posBound;
    }

}
